### Word Scramble Game

### Web Version

![screenshot](web-app.gif)

### Mobile Version (in progress)

![screenshot](scramble.gif)

### Debugging Redux state

Instalation:

https://streetsmartdev.com/debugging-react-native-expo-using-react-native-debugger/

Running:

https://github.com/expo/expo/issues/553#issuecomment-370350423
